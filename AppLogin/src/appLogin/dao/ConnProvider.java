package appLogin.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnProvider {

	public Connection getNewConnection() throws SQLException {
		String driverClass = "org.mariadb.jdbc.Driver";

		String url = "jdbc:mariadb://localhost:3306/loginapp";
		String usuario = "login";
		String password = "login";
		
//		String driverClass = "oracle.jdbc.driver.OracleDriver";
//		String url = "jdbc:oracle:thin:@//172.16.39.200:1521/XE";

		try {
			Class.forName(driverClass);
		} catch (ClassNotFoundException e) {
			System.err.println("No se encuentra el driver JDBC. Revise su configuración.");
			throw new RuntimeException(e);
		}

		Connection conn = DriverManager.getConnection(url, usuario, password);
		return conn;
	}
	
}
