package appLogin.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import appLogin.modelo.Usuario;

public class LoginDao {

	public LoginDao() {
	}
	
	public Boolean validarLogin(Connection connection, Usuario user) throws SQLException {
		Statement stmt = null;
		ResultSet resultSet = null;
		try {
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM usuarios WHERE usuario = '" + user.getUsuario() + "' AND password = '" + user.getPassword() + "';");
			try {
				while (resultSet.next()) {
					if (resultSet.getInt(1) > 0) {
						user.setIdUsuario(resultSet.getInt("id"));
						user.setNombre(resultSet.getString("nombre"));
						user.setApellidos(resultSet.getString("apellidos"));
						user.setPassword(resultSet.getString("password"));
						return true;
					}
				}
			} catch (SQLException e) {
				System.err.println("Error obteniendo los datos.");
			}
		} finally {
			if (stmt != null) {
				stmt.close(); 
			}
		}
		return false;
	}

	public Usuario obtenerUsuario(Connection connection, String nombreUsuario) throws SQLException {
		Statement stmt = null;
		ResultSet resultSet = null;
		try {
			stmt = connection.createStatement();
			resultSet = stmt.executeQuery("SELECT * FROM usuarios WHERE usuario = '" + nombreUsuario + "';");
			try {
				while (resultSet.next()) {
					Usuario user = new Usuario();
					user.setIdUsuario(resultSet.getInt("id"));
					user.setUsuario(resultSet.getString("usuario"));
					user.setNombre(resultSet.getString("nombre"));
					user.setApellidos(resultSet.getString("apellidos"));
					user.setPassword(resultSet.getString("password"));
					return user;
				}
			} catch (SQLException e) {
				System.err.println("Error obteniendo los datos.");
			}
		} finally {
			if (stmt != null) {
				stmt.close(); 
			}
		}
		return null;
	}

	public void actualizarDatos(Connection connection, Usuario usuario) throws SQLException {
		PreparedStatement pStmt = null;
		try {
			pStmt = connection.prepareStatement("UPDATE usuarios SET usuario = ?, password = ?, nombre = ?, apellidos = ? WHERE id = ?;");
			pStmt.setString(1, usuario.getUsuario());
			pStmt.setString(2, usuario.getPassword());
			pStmt.setString(3, usuario.getNombre());
			pStmt.setString(4, usuario.getApellidos());
			pStmt.setInt(5, usuario.getIdUsuario());
			pStmt.execute();
		} finally {
			if (pStmt != null) {
				pStmt.close(); 
			}
		}
	}
}
