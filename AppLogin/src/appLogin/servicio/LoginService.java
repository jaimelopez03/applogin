package appLogin.servicio;

import java.sql.Connection;
import java.sql.SQLException;

import appLogin.dao.ConnProvider;
import appLogin.dao.LoginDao;
import appLogin.modelo.Usuario;

public class LoginService {

	public LoginService() {
	}

	public Boolean validarLogin(Usuario usuario) {
		Connection connection = null;
		try {
			connection = new ConnProvider().getNewConnection();
			
			LoginDao loginDao = new LoginDao();
			if (!usuario.getUsuario().isEmpty() && !usuario.getUsuario().isBlank() &&
					!usuario.getPassword().isEmpty() && !usuario.getPassword().isBlank() &&
					loginDao.validarLogin(connection, usuario)) {
				return true;
			}
		} catch (SQLException e) {
			System.err.println("Error al conectar con la base de datos.");
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception ignore) {
				System.err.println("No se ha podido cerrar la conexión.");
			}
		}
		return false;
	}
	
	public Usuario obtenerUsuario(String nombreUsuario) {
		Connection connection = null;
		try {
			connection = new ConnProvider().getNewConnection();
			
			LoginDao loginDao = new LoginDao();
			Usuario usuario = new Usuario();
			
			usuario = loginDao.obtenerUsuario(connection, nombreUsuario);
			return usuario;
		} catch (SQLException e) {
			System.err.println("Error al conectar con la base de datos.");
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception ignore) {
				System.err.println("No se ha podido cerrar la conexión.");
			}
		}
		return null;
	}
	
	public void actualizarDatos(Usuario usuario) {
		Connection connection = null;
		try {
			connection = new ConnProvider().getNewConnection();
			
			LoginDao loginDao = new LoginDao();
			loginDao.actualizarDatos(connection, usuario);
			
		} catch (SQLException e) {
			System.err.println("Error al conectar con la base de datos.");
			e.printStackTrace();
		} finally {
			try {
				connection.close();
			} catch (Exception ignore) {
				System.err.println("No se ha podido cerrar la conexión.");
			}
		}
	}
}
