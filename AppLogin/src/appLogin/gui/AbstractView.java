package appLogin.gui;

import javax.swing.JPanel;

import appLogin.App;

public abstract class AbstractView extends JPanel {
	private static final long serialVersionUID = -6629380962097598336L;

	public App app;
	
	public abstract void initialize();
}

