package appLogin.gui;

import javax.swing.JLabel;
import javax.swing.JTextField;

import appLogin.App;
import appLogin.modelo.Usuario;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class Datos extends AbstractView {

	private static final long serialVersionUID = 6865298868016984363L;
	
	private JLabel lblUsuario;
	private JLabel lblNombre;
	private JLabel lblApellidos;
	private JTextField txtfldUsuario;
	private JTextField txtfldNombre;
	private JTextField txtfldApellidos;
	private JButton btnModificar;
	private JButton btnSalir;

	public Datos(App app) {
		setLayout(null);
		this.app = app;
		
		lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(65, 70, 75, 14);
		add(lblUsuario);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(65, 100, 75, 14);
		add(lblNombre);
		
		lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(65, 130, 75, 14);
		add(lblApellidos);
		
		txtfldUsuario = new JTextField();
		txtfldUsuario.setEditable(false);
		txtfldUsuario.setBounds(160, 67, 86, 20);
		add(txtfldUsuario);
		txtfldUsuario.setColumns(10);
		
		txtfldNombre = new JTextField();
		txtfldNombre.setEditable(false);
		txtfldNombre.setBounds(160, 100, 86, 20);
		add(txtfldNombre);
		txtfldNombre.setColumns(10);
		
		txtfldApellidos = new JTextField();
		txtfldApellidos.setEditable(false);
		txtfldApellidos.setBounds(160, 130, 86, 20);
		add(txtfldApellidos);
		txtfldApellidos.setColumns(10);
		
		btnModificar = new JButton("Modificar");
		btnModificar.setBounds(160, 215, 89, 23);
		add(btnModificar);
		btnModificar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				app.cambiarVentanaModificar();
			}
		});
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(315, 215, 89, 23);
		add(btnSalir);
		btnSalir.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				app.cambiarVentanaLogin();
			}
		});
		
	}
	
	public void cargarDatos(Usuario usuario) {
		txtfldUsuario.setText(usuario.getUsuario());
		txtfldNombre.setText(usuario.getNombre());
		txtfldApellidos.setText(usuario.getApellidos());
	}
	
	@Override
	public void initialize() {
	}
	
}
