package appLogin.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import appLogin.App;
import appLogin.modelo.Usuario;
import appLogin.servicio.LoginService;

import javax.swing.JButton;

public class Login extends AbstractView {

	private static final long serialVersionUID = 2560074444493837082L;
	
	private JLabel lblUsuario;
	private JLabel lblPassword;
	private JTextField txtfldUsuario;
	private JPasswordField txtfldPassword;
	private JButton btnEntrar;
	private JButton btnSalir;

	public Login(App app) {
		setLayout(null);
		this.app = app;
		
		lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(75, 75, 70, 14);
		add(lblUsuario);
		
		lblPassword = new JLabel("Password");
		lblPassword.setBounds(75, 115, 70, 14);
		add(lblPassword);
		
		txtfldUsuario = new JTextField();
		txtfldUsuario.setBounds(170, 72, 86, 20);
		add(txtfldUsuario);
		txtfldUsuario.setColumns(10);
		
		txtfldPassword = new JPasswordField();
		txtfldPassword.setBounds(170, 112, 86, 20);
		add(txtfldPassword);
		txtfldPassword.setColumns(10);
		
		btnEntrar = new JButton("Entrar");
		btnEntrar.setBounds(315, 175, 89, 23);
		add(btnEntrar);
		btnEntrar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario();
				usuario.setUsuario(txtfldUsuario.getText());
				usuario.setPassword(String.valueOf(txtfldPassword.getPassword()));
				
				LoginService servicio = new LoginService();
				if (servicio.validarLogin(usuario)) {
					app.cambiarVentanaDatos();
					app.guardarUsuario(usuario);
				}
				else {
					JOptionPane.showMessageDialog(txtfldUsuario, "Usuario y/o contraseña incorrectos.");
				}
			}
			
		});
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(315, 215, 89, 23);
		add(btnSalir);
		btnSalir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				app.cerrarVentana();
			}
		});
		
	}

	@Override
	public void initialize() {
	}
}
