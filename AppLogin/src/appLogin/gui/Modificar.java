package appLogin.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import appLogin.App;
import appLogin.modelo.Usuario;
import appLogin.servicio.LoginService;

public class Modificar extends AbstractView {

	private static final long serialVersionUID = -238925903005212048L;
	
	private Integer idUsuario;
	
	private JLabel lblUsuario;
	private JLabel lblNombre;
	private JLabel lblApellidos;
	private JLabel lblPassword;
	private JTextField txtfldUsuario;
	private JTextField txtfldNombre;
	private JTextField txtfldApellidos;
	private JPasswordField txtfldPassword;
	private JButton btnGuardar;
	private JButton btnSalir;

	public Modificar(App app) {
		setLayout(null);
		this.app = app;
		
		lblUsuario = new JLabel("Usuario");
		lblUsuario.setBounds(65, 70, 75, 14);
		add(lblUsuario);
		
		lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(65, 100, 75, 14);
		add(lblNombre);
		
		lblApellidos = new JLabel("Apellidos");
		lblApellidos.setBounds(65, 130, 75, 14);
		add(lblApellidos);
		
		lblPassword = new JLabel("Password");
		lblPassword.setBounds(65, 160, 75, 14);
		add(lblPassword);
		
		txtfldUsuario = new JTextField();
		txtfldUsuario.setBounds(160, 67, 86, 20);
		add(txtfldUsuario);
		txtfldUsuario.setColumns(10);
		
		txtfldNombre = new JTextField();
		txtfldNombre.setBounds(160, 100, 86, 20);
		add(txtfldNombre);
		txtfldNombre.setColumns(10);
		
		txtfldApellidos = new JTextField();
		txtfldApellidos.setBounds(160, 130, 86, 20);
		add(txtfldApellidos);
		txtfldApellidos.setColumns(10);
		
		txtfldPassword = new JPasswordField();
		txtfldPassword.setBounds(160, 160, 86, 20);
		add(txtfldPassword);
		txtfldPassword.setColumns(10);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.setBounds(160, 215, 89, 23);
		add(btnGuardar);
		btnGuardar.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Usuario usuario = new Usuario();
				usuario.setIdUsuario(idUsuario);
				usuario.setUsuario(txtfldUsuario.getText());
				usuario.setPassword(String.valueOf(txtfldPassword.getPassword()));
				usuario.setNombre(txtfldNombre.getText());
				usuario.setApellidos(txtfldApellidos.getText());
				
				LoginService servicio = new LoginService();
				servicio.actualizarDatos(usuario);
				app.cambiarVentanaDatos();
				JOptionPane.showMessageDialog(null, "Datos guardados correctamente.");
			}
		});
		
		btnSalir = new JButton("Salir");
		btnSalir.setBounds(315, 215, 89, 23);
		add(btnSalir);
		btnSalir.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				app.cambiarVentanaDatos();
			}
		});
	}
	
	public void cargarDatos(Usuario usuario) {
		txtfldUsuario.setText(usuario.getUsuario());
		txtfldNombre.setText(usuario.getNombre());
		txtfldApellidos.setText(usuario.getApellidos());
		txtfldPassword.setText(usuario.getPassword());
		idUsuario = usuario.getIdUsuario();
	}

	@Override
	public void initialize() {
	}
}
