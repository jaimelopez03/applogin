package appLogin;

import java.awt.EventQueue;

import javax.swing.JFrame;

import appLogin.gui.AbstractView;
import appLogin.gui.Datos;
import appLogin.gui.Login;
import appLogin.gui.Modificar;
import appLogin.modelo.Usuario;

public class App {
	
	private JFrame ventana;
	private Login login;
	private Modificar modificar;
	private Datos datos;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					App app = new App();
					app.ventana.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public App() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		ventana = new JFrame();
		ventana.setBounds(100, 100, 450, 300);
		ventana.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		ventana.setResizable(false);
		
		login = new Login(this);
		modificar = new Modificar(this);
		datos = new Datos(this);

		ventana.setContentPane(login);
		login.setVisible(true);
	}

	private void cambiarView(AbstractView view) {
		ventana.setContentPane(view);
		view.setVisible(true);
		ventana.revalidate();
	}
	
	public void cambiarVentanaLogin() {
		cambiarView(login);
	}
	public void cambiarVentanaDatos() {
		cambiarView(datos);
	}
	public void cambiarVentanaModificar() {
		cambiarView(modificar);
	}
	public void cerrarVentana() {
		System.exit(0);
	}
	public void guardarUsuario(Usuario usuario) {
		datos.cargarDatos(usuario);
		modificar.cargarDatos(usuario);
	}
}
